const mongoose = require("mongoose");

const userSchema = new mongoose.Schema({
    firstName: {
        type: String,
        required: [true, "Your first name is required."]
    },
    lastName: {
        type: String,
        required: [true, "Your last name is required."]
    },
    email: {
        type: String,
        required: [true, "Please add your email address."]
    },
    password: {
        type: String,
        required: [true, "Password is required."]
    },
    isAdmin: {
        type: Boolean,
        default: false,
        required: [true, "Please let us know if you are an admin."]
    },
    mobileNo: {
        type: String,
        required: [true, "Please enter your contact number."]
    },
    registeredOn: {
        type: Date,
        default: new Date()
    },
    enrollments: [
        {
            courseId: {
                type: String,
                required: [true, "Your chosen courseId is required."]
            },
            enrolledOn: {
                type: Date,
                default: new Date()
            },
            status: {
                type: String,
                default: "Enrolled"
            }
        }
    ]
});

module.exports = mongoose.model("User", userSchema);